
export type Question = {
    name: string,
    longName: string,
    id: string,
    value: number,
}

export type Ballot = {
    id: string,
    name: string,
    userId: string,
    questions: Question[],
}

export type QuestionTally = {
    name: string,
    longName: string,
    questionId: string,
    answers: {userId: string, value: number}[]
}

export type BallotTally = {
    ballotId: string,
    name: string,
    questionsTally: QuestionTally[],

}

function defaultQuestions(): Question[]{
    return [
        {id: "5242", name: "readme", value: 25, longName: "Qualité du README"},
        {id: "shs72g3", name: "diagrams", value: 50, longName: "Qualité des diagrammes"},
        {id: "y76sfg", name: "convinced", value: 75, longName: "Êtes-vous convaincu?"},
    ];
}


export function createDefaultBallots(userId: string): Ballot[]{
    return [
        {id: "5gg64", name: "Les chacals de laval", userId: userId, questions: defaultQuestions()},
        {id: "ys68", name: "Les rapides de blainville",  userId: userId, questions: defaultQuestions()},
        {id: "ssw", name: "Les pas-beaux du plateau",  userId: userId, questions: defaultQuestions()},
    ];
}

