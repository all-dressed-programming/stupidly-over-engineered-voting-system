import { Application, Router } from "https://deno.land/x/oak/mod.ts";
import {createBundle} from "./bundler.ts";
import {Context} from "https://deno.land/x/oak@v12.2.0/context.ts";
import {Ballot, createDefaultBallots, Question, BallotTally, QuestionTally} from "./shared.ts";
import {verifyAuth} from "./auth.ts";

type QuestionAnswer = {
    value: number
}

const router = new Router();

const prodClientBundle = createBundle(
    new URL("./client.ts", import.meta.url),
    new URL("../import_map.json", import.meta.url),
    false,
);



type IndexPrefix = ["soevs", "v1"];

type UserPrefix = [... IndexPrefix, "user", userId: string];

type UserBallotQuestionIndex = [...UserPrefix, "ballot", ballotId: string, "question", questionId: string];

type BallotIndex = [...IndexPrefix, "ballot", ballotId: string];

type BallotQuestionIndex = [...BallotIndex , "question", questionId: string];

type BallotQuestionUserIndex = [...BallotQuestionIndex, "user", userId: string];

function getIndexPrefix(): IndexPrefix{
    return ["soevs", "v1"];
}

function getUserIndex(userId: string): UserPrefix {
    const prefix = getIndexPrefix();
    return [...prefix, "user", userId];
}

function getBallotIndex(ballot: Ballot): BallotIndex{
    const prefix = getIndexPrefix();
    return [...prefix, "ballot", ballot.id];
}

function getBallotIndexByBallotId(ballotId: string): BallotIndex{
    const bogusBallot: Ballot = createDefaultBallots('bogus').find(x => x.id === ballotId)!;
    return getBallotIndex(bogusBallot);
}

function getBallotQuestionIndex(ballot: Ballot, question: Question): BallotQuestionIndex{
    const prefix = getBallotIndex(ballot);
    return [...prefix, "question", question.id];
}

function getBallotQuestionUserIndex(userId: string, ballot: Ballot, question: Question): BallotQuestionUserIndex{
    const prefix = getBallotQuestionIndex(ballot, question);
    return [...prefix, "user", userId];
}

function getUserBallotQuestionIndex(userId: string, ballot: Ballot, question: Question): UserBallotQuestionIndex{
    const prefix = getUserIndex(userId);
    return [...prefix, "ballot", ballot.id, "question", question.id];
}

async function persistBallots(userId: string, ballots: Ballot[]): Promise<void> {
    const kv = await Deno.openKv();
    for(const ballot of ballots){
        for(const question of ballot.questions){
            const v: QuestionAnswer = {value: question.value};
            await kv.set(getUserBallotQuestionIndex(userId, ballot, question), v);
            await kv.set(getBallotQuestionUserIndex(userId, ballot, question), v)
        }
    }
}

function setValueInQuestion(ballots: Ballot[], key: UserBallotQuestionIndex, value: number){
    const questionId = key[7]; // UserBallotQuestionIndex.questionId
    const ballotId = key[5]; // UserBallotQuestionIndex.ballotId
    for(const ballot of ballots){
        for(const question of ballot.questions){
            if(question.id === questionId && ballot.id === ballotId){
                question.value = value;
            }
        }
    }
    return ballots;
}

async function getUserBallots(userId: string): Promise<Ballot[]>{
    let ballots = createDefaultBallots(userId);
    const kv = await Deno.openKv();
    const entries = kv.list<QuestionAnswer>({ prefix: getUserIndex(userId) });
    for await (const entry of entries) {
        const key = entry.key;
        const value = entry.value.value;
        ballots = setValueInQuestion(ballots, key as UserBallotQuestionIndex, value);
    }
    return ballots;
}

function createEmptyResult(b: Ballot): BallotTally {
    return {
        ballotId: b.id,
        name: b.name,
        questionsTally: [],
    };
}

async function compactifyQuestionTally(tallies: QuestionTally[]): Promise<QuestionTally[]>{
    const ret: QuestionTally[] = [];
    for(const subTallies of Object.values(Object.groupBy(tallies, (t) => t.questionId))){
        ret.push({
            questionId: subTallies[0].questionId,
            name: subTallies[0].name,
            longName: subTallies[0].longName,
            answers: subTallies.map(t => t.answers).flat(),
        });
    }
    return ret;
}

function getQuestionsMappedById(ballotId: string): Record<string, Question>{
    const templateBallot: Ballot = createDefaultBallots('bogus').find(x => x.id === ballotId)!;
    return templateBallot.questions.reduce((acc, x: Question) => {acc[x.id] = x; return acc}, {});
}

function createSimpleQuestionTally(question: Question, userId: string, value: number): QuestionTally{
    return {
        questionId: question.id,
        name: question.name,
        longName: question.longName,
        answers: [{userId, value}],
    }
}
async function* getBallotsByBallotId(ballotId: string): AsyncGenerator<QuestionTally>{
    const questions = getQuestionsMappedById(ballotId);
    const kv = await Deno.openKv();
    const entries = kv.list<QuestionAnswer>({ prefix: getBallotIndexByBallotId(ballotId) });
    for await (const entry of entries) {
        const key = entry.key as BallotQuestionUserIndex;
        const questionId = key[5];
        const question = questions[questionId];
        yield createSimpleQuestionTally(question, key[7], entry.value.value);
    }
}

async function getQuestionTallies(ballotId: string): Promise<QuestionTally[]> {
    const ballots = getBallotsByBallotId(ballotId);
    return compactifyQuestionTally(await  Array.fromAsync(ballots));
}

async function getBallotTallies(userId: string): Promise<BallotTally[]>{
    const ret = createDefaultBallots(userId).map(createEmptyResult);
    for(const res of ret) {
        res.questionsTally = await getQuestionTallies(res.ballotId);
    }
    return ret;
}

router.post("/api/v1/vote/:userid", async (ctx): Promise<void> => {
    const userId: string = ctx?.params?.userid??"";
    if(!await verifyAuth(userId)){
        ctx.throw(403)
    }
    if(!ctx.request.hasBody){
        ctx.throw(415);
    }
    const body = await ctx.request.body().value;
    const ballots = body as Ballot[];
    await persistBallots(userId, ballots);
    ctx.response.body = userId;
});

router.get("/api/v1/vote/:userid", async (ctx)=> {
    const userId = ctx?.params?.userid??"";
    if(!await verifyAuth(userId)){
        ctx.throw(403)
    }
    ctx.response.body = await getUserBallots(userId);
});

router.get("/api/v1/results", async (context): Promise<void> => {
    const userId = '1234';
    context.response.body = await getBallotTallies(userId);
});

router.get("/client.js", async (context): Promise<void> => {
    context.response.body = await prodClientBundle;
    // context.response.body = await createBundle(
    //     new URL("./client.ts", import.meta.url),
    //     new URL("../import_map.json", import.meta.url),
    //     true,
    // );
    context.response.type = "application/javascript";
});

router.get("/", (context) => {
    context.response.body = `
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>title</title>
        <link href="https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/vega@5.26.1"></script>
        <script src="https://cdn.jsdelivr.net/npm/vega-lite@5.16.3"></script>
        <!-- Import vega-embed -->
        <script src="https://cdn.jsdelivr.net/npm/vega-embed@6.23.0"></script>
    </head>
    <body>
      <script type="module" src="client.js"></script>
    </body>
    </html>
    `;
});




const app = new Application();
app.use(router.routes());


app.use((ctx) => {
    ctx.response.body = "Hello World!";
});

await app.listen({ port: 3333 });




