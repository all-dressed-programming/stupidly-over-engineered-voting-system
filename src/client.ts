// @deno-types="https://esm.sh/@types/mithril@2.2.4"
import m from "https://esm.sh/mithril@2.2.2"
import {verifyAuth} from "./auth.ts";
import {Ballot, BallotTally, Question, QuestionTally} from "./shared.ts";
import {quantile, min, max} from 'https://esm.sh/simple-statistics@7.8.3'

let ballots: Ballot[] = [];
function isAdmin(): boolean{
    return  window.localStorage.getItem("isAdmin") === 'true';
}

type ProgressBarAttr = {
    question: Question,
    onchange: () => void,
}

function getBallotURL(): string{
    const userId = window.localStorage.getItem("userId");
    if(!userId){
        m.route.set("/");
        throw new Error("User not logged in");
    }
    return `/api/v1/vote/${userId}`;
}

class ProgressBar implements m.ClassComponent<ProgressBarAttr>{

    closestFivePercent(n: number): number{
        let ret = 0;
        let distRet = 101;
        for(let i = 0; i < 101; i += 5){
            if(Math.abs(i - n) < distRet) {
                ret = i;
                distRet = Math.abs(i - n);
            }
        }
        return ret;
    }

    computeClickedScored(event: Event){
        const total = event.target.clientWidth;
        const score = event.x - event.target.offsetLeft;
        return this.closestFivePercent(( score / total ) * 100);
    }
    view({attrs}: m.CVnode<ProgressBarAttr>){
        const onclickFn = (e: Event) => {
            attrs.question.value = this.computeClickedScored(e);
            attrs.onchange();
        }
        const value = attrs.question.value;
        return m("progress.progress.is-primary",
            {value: value, max: 100, onclick: onclickFn, title: `${value}%`},
            `${value}%`)
    }
}

type BallotViewAttr = {
    ballot: Ballot,
    onchange?: () => void,
};

class BallotView implements m.ClassComponent<BallotViewAttr> {

    voteFor(question: Question, onchange: () => void){
        return m("div.columns",
            m("div.column", question.longName, ":"),
            m("div.column.is-two-thirds",
                m(new ProgressBar(), {question: question, onchange: onchange})));
    }

    view(vnode: m.CVnode<BallotViewAttr>){
        const onchange = vnode.attrs.onchange ?? function(){};
        const questions = vnode.attrs.ballot.questions??[];
        return questions.map(question => this.voteFor(question, onchange));
    }
}

class Submit implements m.ClassComponent<unknown> {
    view(){
        return m("div.columns",
            m("div.column",
                m(m.route.Link, {class: "button", href: "/voter-home/"}, `Votez!`),
        ));
    }
}

type MainVoteViewAttr = {
    ballotId: number,
}

class MainVoteView implements m.ClassComponent<MainVoteViewAttr>{

    async persistBallots(): Promise<void>{
        const params: RequestInit = {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(ballots)
        };
        const resp = await fetch(getBallotURL(), params);
        console.log(resp.status)
    }

    emptyState(): m.Vnode{
        return m("div", "No ballot found");
    }

    view(vnode: m.CVnode<MainVoteViewAttr>): m.Vnode {
        const ballot = ballots.find( ballot => ballot.id.toString() === vnode.attrs.ballotId.toString());
        if(!ballot){
            return this.emptyState();
        }
        return m("div.columns.is-centered",
            m("div.column.is-two-thirds",
                m("h1.title.is-1", m(m.route.Link, {href: "/voter-home/"}, `Votons - ${ballot.name}`)),
                m(new BallotView(), {ballotId: vnode.attrs.ballotId,
                                     ballot: ballot,
                                     onchange: this.persistBallots}),
                m(new Submit())));
    }
}

class MainView implements m.ClassComponent<unknown>{

    createBallotRow(ballot: Ballot){
        const ballotURL = `${m.route.get()}vote/${ballot.id}/`;
        return m('tr',
            m('td', m(m.route.Link, {href: ballotURL}, ballot.name)),
            ballot.questions.map(q => m("td", q.value)));
    }

    votesTable(ballots: Ballot[]){
        return m('table.table.is-striped.is-fullwidth',
            m('thead',
                m('tr', ['Équipe', 'Readme', 'Diagrammes', 'Idée'].map(title => m('th', title)))),
            m('tbody', ballots.map(ballot => this.createBallotRow(ballot))))
    }
    view(){
        return m("div.columns.is-centered",
            m("div.column.is-two-thirds",
                this.votesTable(ballots)));
    }
}

class PasscodeView implements m.ClassComponent<unknown> {

    async verifyPasscode(event: Event): Promise<void> {
        let password = event.target.value;
        let user = await verifyAuth(password)
        if(user){
            window.localStorage.setItem("userId", password);
            window.localStorage.setItem("isAdmin", user.isAdmin);
            m.route.set(`/voter-home/`);
        }
    }

    view(){
        const change = (e) => this.verifyPasscode(e);
        return m('div.hero.is-fullheight',
            m("div.hero-body.has-text-centered",
                m("div.container",
                    m("div.box", m("input.input", {placeholder: "passcode", onkeyup: change})))));
    }
}

type StupidBoxPlotViewAttr = {
    questionTally: QuestionTally,
}

class StupidBoxPlotView implements m.ClassComponent<StupidBoxPlotViewAttr>{
    vegaLightSchema(lower, q1, media, q3, upper){
        return {
            "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
            "data": {
            "values": [{
                lower, q1, media, q3, upper
            }]
        },
            "layer": [
            {
                "mark": {"type": "rule"},
                "encoding": {
                    "y": {"field": "lower", "type": "quantitative","scale": {"zero": false}, "title": null},
                    "y2": {"field": "upper"}
                }
            },
            {
                "mark": {"type": "bar", "size": 14},
                "encoding": {
                    "y": {"field": "q1", "type": "quantitative"},
                    "y2": {"field": "q3"},
                }
            },
            {
                "mark": {"type": "tick", "color": "white", "size": 14},
                "encoding": {
                    "y": {"field": "median", "type": "quantitative"}
                }
            },
        ]
        }

    }

    oncreate(vnode: m.CVnode<StupidBoxPlotViewAttr>){
        const data = vnode.attrs.questionTally.answers.map(x => x.value)
        if(data.length < 1) {
            return;
        }

        const shema = this.vegaLightSchema(min(data),
                                             quantile(data, 0.25),
                                             quantile(data, 0.5),
                                             quantile(data, 0.75),
                                             max(data));
        window.vegaEmbed( vnode.dom, shema)
    }
    view(){
        return m("div", "")
    }
}

class ResultView implements m.ClassComponent<unknown>{
    ballotTallies: BallotTally[] = [];
    allowedToView = true;

    async oninit(): Promise<void> {
        this.allowedToView = isAdmin()
        if(!this.allowedToView){
            this.allowedToView = false;
            return;
        }
        const userId = window.localStorage.getItem("userId");
        this.ballotTallies = await m.request(`/api/v1/results`);
    }

    resultTablesHeaders(bts: BallotTally[]):  m.Attributes{
        return m('thead',
            m("tr", m("th"),  bts.map(bt => m("th", {colspan: bt.questionsTally.length}, bt.name))),
            m("tr", m("th", "User"), bts.flatMap(bt => bt.questionsTally.map(qt => m("th", qt.name)))),
            );
    }

    getUserIds(bts: BallotTally[]): Set<string>{
        const ret = bts.flatMap(bt => bt.questionsTally)
            .flatMap(qt => qt.answers)
            .map(a => a.userId);
        return new Set(ret)
    }

    getUserResult(bt: BallotTally, userId: string): string[]{
        return bt.questionsTally.map(qt => qt.answers.find(a => a.userId === userId)?.value?.toString()??"")
    }

    *getResultRowData(bts: BallotTally[]): Generator<{data: string[], userId: string }>{
        for(const userId of this.getUserIds(bts)){
            const data =  bts.flatMap(bt => this.getUserResult(bt, userId));
            yield {data, userId};
        }
    }

    userCell(userId: string): m.Attributes{
        return m('th', {style: {color: 'transparent', 'text-shadow': '0 0 8px #000'}, title: userId},  userId)
    }
    resultBody(bts: BallotTally[]):  m.Attributes{
        const rowData = Array.from(this.getResultRowData(bts));
        return m('tbody', rowData.map(data => m("tr", this.userCell(data.userId), data.data.map(d => m("td", d))) ));
    }
    stupidBoxPlots(ballotTallies: BallotTally[]): m.Attributes {
        return m('tbody',
            m('tr', m('td'),
                ballotTallies.flatMap(bt => bt.questionsTally)
                    .map(qt => m('td', m(StupidBoxPlotView, {questionTally: qt})))));
    }

    resultTables(ballotTallies: BallotTally[]): m.Attributes {
        return m('table.table.is-fullwidth.is-bordered',
            [this.resultTablesHeaders(ballotTallies),
             this.resultBody(ballotTallies),
             this.stupidBoxPlots(ballotTallies)
            ])
    }

    view(){
        if(!this.allowedToView){
            return m('div.hero.is-fullheight',
                m("div.hero-body.has-text-centered",
                    m("div.container",
                        m("h1.title.is-1", ":( you are not allow to view this page"))));
        }
        return m("div", this.resultTables(this.ballotTallies));
    }

}

class FrameView<T extends m.ClassComponent<any>> implements m.ClassComponent<unknown>{

    private readonly embedeed: new () => T;

    constructor(embedeed: new () => T) {
        this.embedeed = embedeed
    }

    async oninit(): Promise<void>{
        ballots = await m.request(getBallotURL());
    }

    logout(){
        window.localStorage.clear();
        m.route.set("/");
    }
    resultButton(){
        if(!isAdmin()){
            return m("div");
        }
        return m(m.route.Link, {class: "button", href: "/results"}, `Results`)
    }

    view(vnode: m.Vnode<unknown>): m.Children {
        return m("div", [
            m("nav.navbar.is-primary", [
                m("div.navbar-item",
                    m("img", {src: "https://all-dressed-programming.com/images/pizza.svg"}),
                    m("span.ml-1.title.is-5", "Stupidly Over Engineered Voting System")),
                m("div.navbar-end",
                    m("div.navbar-item",
                        this.resultButton()),
                    m("div.navbar-item" ,
                        m("a.is-clickable.button", {onclick: () => this.logout()},  "logout")))
            ]),
            m(this.embedeed, vnode.attrs)
        ]);
    }
}


m.route(window.document.body, "/", {
    "/": new FrameView(PasscodeView),
    "/voter-home/": new FrameView(MainView),
    "/results": new FrameView(ResultView),
    "/voter-home/vote/:ballotId/": new FrameView(MainVoteView)
})