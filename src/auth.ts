import { encodeBase64 } from "https://deno.land/std/encoding/base64.ts"

const salt = "Honey?I_am/home";

type User = {
    hash: string,
    isAdmin: boolean,
}
const users: User[] = [
    {hash: "2jztmK1WMRc4ZSUN/jxbnsO/IjlWMFvxh1i/AMeLM3c=", isAdmin: true},
    {hash: "mipaAgOTc14kkI8oJltUCJjoUEBufS79H8IJK1w3zBE=", isAdmin: false},
]

export async function hash(msg: string): Promise<string>{
    const enc = new TextEncoder();
    const key = await crypto.subtle.importKey(
        "raw",
        enc.encode(salt),
        {name: "HMAC", hash: "SHA-256"},
        true,
        ["sign", "verify"],
    )
    const ret = await crypto.subtle.sign("HMAC", key,  enc.encode(msg));
    return encodeBase64(new Uint8Array(ret));
}

function veryInsecureEquality(a: string, b: string): boolean{
    return a === b;
}

export async function verifyAuth(auth: string): Promise<User|null>{
    let ret: User|null = null;
    const authHash = await hash(auth);
    for (const user of users) {
        if (veryInsecureEquality(user.hash, authHash)) { // do not do that at home
            ret = user;
        }
    }
    return ret;
}


console.log(await hash("2468"))