/**
 * This part of the code is based on the way `fresh` is bundling the code.
 * See https://github.com/denoland/fresh/blob/main/src/server/bundle.ts
 */

import * as esbuild from "https://deno.land/x/esbuild@v0.19.2/mod.js";

import { denoPlugins } from "https://deno.land/x/esbuild_deno_loader@0.8.2/mod.ts";


let esbuildInitialized = false;
async function verifyEsbuildInitialized(): Promise<void> {
    if (esbuildInitialized) {
        return;
    }
    console.log(new Date(), "start initializing esbuild");
    await esbuild.initialize({});

    esbuildInitialized = true;
    console.log(new Date(), "esbuild initialized");
}

export async function createBundle(
    entryPoint: URL,
    importMapURL: URL,
    isDev?: boolean,
) {
    await verifyEsbuildInitialized();
    console.log(new Date(), "Bundling", entryPoint.href);
    const absWorkingDir = Deno.cwd();
    const res = await esbuild.build({
        bundle: true,
        entryPoints: [entryPoint.href],
        platform: "neutral",
        treeShaking: true,
        minify: !isDev,
        sourcemap: isDev ? "inline" : false,
        outfile: "",
        absWorkingDir: Deno.env.get('XDG_CACHE_HOME') ,
        outdir: ".",
        target: ["chrome99", "firefox99", "safari15"],
        write: false,
        format: "esm",
        plugins: [...denoPlugins({ importMapURL: importMapURL.href })],
        splitting: false,
        jsx: "automatic",
        jsxImportSource: "react",
    });
    if (res.errors.length > 0) {
        throw res.errors;
    }
    console.log(new Date(), "Finished bundling", entryPoint.href);
    return res.outputFiles[0].text;
}

